
const random = require("random-js")();

const pickRandomFromArray = function (array) {
  let index = 0;
  let swapIndex = 0;

  for (; index < array.length; index++) {
    swapIndex = Math.floor(Math.random() * (index + 1));
    [array[index], array[swapIndex]] = [array[swapIndex], array[index]];
  }

  return array;
};

module.exports = { pickRandomFromArray };