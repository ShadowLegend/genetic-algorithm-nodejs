
const minToMaxRate = function (gene, customCompareFunction = null) {
  let result = .0;
  let length = gene.length - 1;
  let index = 0;

  for (; index < length; ++index) {
    if (gene[index] < gene[index + 1]) {
      ++ result;
    }
  }

  return length !== 0 ? result / length : 0;
};

module.exports = { minToMaxRate };
