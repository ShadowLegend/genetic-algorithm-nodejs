
const GeneticAlgorithm = require("./geneticAlgorithm");
const { minToMaxRate } = require("./fitnessFunction");

const defaultPopulationSize = {

  atomic: 10,
  small: 30,
  medium: 100,
  large: 300,
  extreme: 1000

};

const shortestRouteSolver = function (json, size = defaultPopulationSize.small, mutationRate = .1) {

  const populationSize = json.size < defaultPopulationSize.atomic ? size : json.size;

  let GA = new GeneticAlgorithm(populationSize, json, minToMaxRate, mutationRate);
  let loop = 0;

  while (GA._bestFitness < .99 && loop < 5) {

    GA.newGeneration();
    // console.log ("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    // console.log ( "Generation: ", GA._generation );
    // console.log ( "Current Fitness Score: ", GA._bestFitness );
    // console.log ( "Current Best Gene: ", GA._bestGenes );
    // console.log ( "Current Population: ", GA._population.getHeadNode() );
    // console.log ("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

    ++ loop;
  }

  return GA;

};

module.exports = { shortestRouteSolver };