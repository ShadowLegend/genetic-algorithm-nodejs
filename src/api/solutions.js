
const Solution = require("../models/solutions");
const Promise = require("promise");

const addSolution = function (args) {

  return new Promise(function (resolve, reject) {

    try {

      const solution = Solution.create(args);

      if (solution) {

        resolve(solution);

      }

    } catch (error) {

      reject(error);

    }

  });

};

const findSolution = function (args = {}) {

  return new Promise(function (resolve, reject) {

    try {

      const solutions = Solution.findOne(args);

      if (solutions) {

        resolve(solutions);

      }

    } catch (error) {

      reject(error);

    }

  });

};

module.exports = { addSolution, findSolution };