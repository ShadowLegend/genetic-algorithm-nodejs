
const questions = [{

    type: 'list',
    name: 'problem',
    message: 'Problem type:',
    choices: [ 'shortest-route', 'lowest-cost' ],
    default: 'shortest-route'

}];

module.exports = questions;