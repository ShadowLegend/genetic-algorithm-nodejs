
const mongoose = require("mongoose");

const SolutionSchema = new mongoose.Schema({

  problemType: {

    type: String

  },

  dataSize: {

    type: Number

  },

  solutions: {

    type: []

  }

}, { timestamps: true });

module.exports = mongoose.model("Solution", SolutionSchema);