
const random = require("random-js");
const { pickRandomFromArray } = require("./randomGene");
const fs = require("fs");
const outputFile = 'output.txt';

class DNA {
  constructor(size, genesis, fitnessFunction, init = false) {
    this._genes = null;
    this._genesis = genesis.slice();
    this._length = size;
    this._fitness = .0;
    this.getRandomGeneFunc = pickRandomFromArray;
    this.fitnessFunction = fitnessFunction;

    if (init) {
      this._genes = this.getRandomGeneFunc(this._genesis.slice());
    } else {
      this._genes = new Array(size);
    }
  };

  calculateFitness() { return this._fitness = this.fitnessFunction(this.getGenes()); }

  getSize() { return this._length; }

  getGenes() { return this._genes.slice(); }

  inspectGeneAt(index) { return this._genes[index]; }

  crossOver(parent) {
    let size = this.getSize();
    let child = new DNA(size, this._genesis, this.fitnessFunction, false);

    let randomNumber = random.integer(1, size);
    let elemToMutate = randomNumber > 3 ? parent._genes.slice(randomNumber, randomNumber + 3) : parent._genes.slice(randomNumber, randomNumber << 1);

    let index = 0;
    let tempIndex = null;
    let tempValue = null;
    let value = null;

    for (; index < size; ++index) {
      value = this._genes[index];

      if (elemToMutate.indexOf(value) >= 0) {
        tempIndex = parent._genes.indexOf(value);
      }

      if (tempIndex) {
        tempValue = this._genes[tempIndex];
        child._genes[index] = tempValue;
        child._genes[tempIndex] = value;
        tempIndex = 0;
      } else {
        child._genes[index] = value;
      }
    }

    // child._genes = child._genes.reduce((crossed, gene, index) => {
    //   crossed[index] = random.integer(0, 1) < .5 ? this.inspectGeneAt(index) : parent.inspectGeneAt(index);
    //   return crossed;
    // }, new Array(size));

    // this._outputHandle(child._genes);
    return child;
  };

  mutation(mutation_rate) {
    if (random.real(mutation_rate, .9) > .5) {
      let randomNum1 = random.integer(0, this._length);
      let randomNum2 = random.integer(0, this._length);

      let temp = this._genes[randomNum1];
      this._genes[randomNum1] = this._genes[randomNum2];
      this._genes[randomNum2] = temp;
    }
  }

  _outputHandle(array, label) {
    const self = this;

    if (fs.existsSync(outputFile)) {
      fs.unlink(outputFile, function(error) {
        if (error) throw new Error(error);

        self._writeToFile(array, label);
      });
    } else {
      self._writeToFile(array, label);
    }
  }

  _writeToFile(array, label) {
    const stream = fs.createWriteStream(outputFile, {flags: 'a'});

    array.forEach(function(element) {
      stream.write(`${label}: ${element}\n`);
    });

    stream.end();
  }
};

module.exports = DNA;
