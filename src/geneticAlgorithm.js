
const random = require("random-js")();
const LinkedList = require("dbly-linked-list");
const _ = require("lodash");
const DNA = require("./dna");

class GeneticAlgorithm {
  constructor(populationSize, json, fitnessFunction, mutationRate = .01) {
    this._population = new LinkedList();
    this._generation = 0;
    this._fitnessSum = .0;
    this._bestFitness = .0;
    this._bestGenes = null;
    this._genesis = json.data;

    this.mutationRate = mutationRate;
    let index = 0;

    for (; index < populationSize; ++index) {
      this._population.insert(new DNA(json.size, this._genesis, fitnessFunction, true));
    }
  }

  newGeneration() {
    if (this._population.getSize() <= 0) return;

    this.calculateFitness();

    let newPopulations = new LinkedList();
    let index = 0;
    let parentOne = null, parentTwo = null, child = null;

    for (; index < this._population.getSize(); ++index) {
      while (!parentOne) {
        parentOne = this._chooseParent();
      }
      while (!parentTwo) {
        parentTwo = this._chooseParent();
      }

      child = parentOne.crossOver(parentTwo);

      child.mutation(this.mutationRate);
      newPopulations.insert(child);
    }

    this._population = newPopulations;
    ++this._generation;
    return this;
  }

  calculateFitness() {
    let index = 0;
    let bestGene = this._population.getHeadNode().getData();
    let gene = null;

    for (; index < this._population.getSize(); ++index) {
      gene = this._population.findAt(index).getData();
      this._fitnessSum += gene.calculateFitness();

      if (gene._fitness > bestGene._fitness) {
        bestGene = gene;
      }
    }

    this._bestFitness = bestGene._fitness;
    this._bestGenes = bestGene.getGenes();

    return this;
  }

  _chooseParent() {
    let sum = this._fitnessSum ? this._fitnessSum : .0;
    let randomNumber = random.real(0, 1);
    randomNumber *= sum;

    let index = 0;
    let gene = null;

    for (; index < this._population.getSize(); ++index) {
      gene = this._population.findAt(index).getData();

      console.log(gene._fitness);

      if (randomNumber <= gene._fitness) {
        return gene;
      }

      randomNumber -= gene._fitness;
    }

    return gene;
  }
};

module.exports = GeneticAlgorithm;
