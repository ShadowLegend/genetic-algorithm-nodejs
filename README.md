# **Genetic Algorithm - NodeJS**

[![build status](https://gitlab.com/ShadowLegend/genetic-algorithm-nodejs/badges/master/build.svg)](https://gitlab.com/ShadowLegend/genetic-algorithm-nodejs/commits/master)
[![Overall test coverage](https://gitlab.com/ShadowLegend/genetic-algorithm-nodejs/badges/master/coverage.svg)](https://gitlab.com/ShadowLegend/genetic-algorithm-nodejs/pipelines)
[![JavaScript coverage](https://gitlab.com/ShadowLegend/genetic-algorithm-nodejs/badges/master/coverage.svg?job=karma)](https://gitlab.com/ShadowLegend/genetic-algorithm-nodejs/coverage-javascript)

## Genetic algorithm to solve optimization problem based on specific problem type

### GA emulates gene evolution process to find solution, it uses Chromosome,
### Crossover, Mutation, Survival of fitness ( natural evolution ) to find
### solution.

### With Genetic Algorithm, it is not necessary to know how to solve the problem,
### but to be able to evaluate the quality of those solutions generated from
### iteration to get good solution.

### Because this GA purpose was to solve problem based on specific problem type, 
### the fitness function are implement differnetly based on each problem type.

### High level understanding:

+ Start with randomly generated K chromosomes - called population
+ Each chromosomes represent individual solution
+ The chromosomes crossover and goes through mutation to create K offspring.
+ Among K population and K offspring, depending on problem type, the fitness
  function will be used to detect best k offsprings

### Pseudo Code

```
    Generate random K chromosomes
    
    Asynchronously generate N chromosomes through crossover, M chromosomes
    through mutation, then take P new random chromosomes which N + M + P = N
    
    Select best K chromosomes from K * C chromosomes, C is some constant of
    iteration
    
    If iteration count < threshold, repeat the process to find better solution
```

### Installation ( Will publish to npm after completed the project )

```
    npm i -g gene
```

### Usage

```
    gene solve <json-data> -t <problem-type>
    
    eg: gene solve "data.json" -t "evolution"
    
    or
    
    eg: gene solve "data.json"
    
    ? Problem Type:
    -> evolution ( default )
       fast-growth
```

### Progress

- [x] Setup cli boilerplate
- [x] API setup
    - [x] Connect MongoDB and define models
    - [x] API to save solution
    - [x] API to view solution
- [x] Implement GA
    - [x] Interface
    - [x] Implement Crossover
    - [x] Implement Population
    - [x] Implement Mutation
    - [x] Implement Fitness functions
    
### MIT LICENSE