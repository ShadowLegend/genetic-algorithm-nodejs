
const mongoose = require("mongoose");
const _ = require('lodash/core');

const { addSolution, findSolution } = require("./src/api/solutions");
const Solution = require("./src/models/solutions");

mongoose.Promise = global.Promise;

try {

  mongoose.connect("mongodb://127.0.0.1/gene");
  Solution.remove({}); // reset database

} catch (error) {

  mongoose.createConnection("mongodb://127.0.0.1/gene");

}

afterAll(function () {

  mongoose.disconnect();

});

test("Add solution test", async function () {

  const response = await addSolution({

    problemType: "shortest-path",
    dataSize: 5,
    solutions: [1, 2, 3, 5, 6]

  });

  expect(response.problemType).toBe("shortest-path");
  expect(response.dataSize).toBe(5);
  expect(_.isEqual(response.solutions, [1, 2, 3, 5, 6])).toBe(true);

});

test("Find shortest path solutions", async function () {

  const response = await findSolution({ problemType: "shortest-path" });

  expect(response.problemType).toBe("shortest-path");
  expect(response.dataSize).toBe(5);
  expect(_.isEqual(response.solutions, [1, 2, 3, 5, 6])).toBe(true);

});