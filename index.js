
// #!/usr/bin/env node

const program = require("commander");
const { prompt } = require("inquirer");

const chalk = require("chalk");
const mongoose = require("mongoose");
const jsonFile = require("jsonfile");

const questions = require("./src/questions");

const { shortestRouteSolver } = require("./src/solvers");

mongoose.Promise = global.Promise;

// try {
//   mongoose.connect("mongodb://127.0.0.1/gene");
// } catch (error) {
//   try {
//     mongoose.createConnection("mongodb://127.0.0.1/gene");
//   } catch (fatal) {
//     if (fatal) process.exit(1);
//   }
// }

// mongoose
//   .connection
//   .once("open", function () { })
//   .on("error", function (error) { throw error; });

program
  .version("0.1.0", "-v, --version")
  .name("gene")
  .usage("<path to json data> [options]")
  .option("-t, --type <type>", "specify problem type")
  .description("Genetic Algorithm to solve optimization problem based on specific problem type");

program
  .command("solve <path>")
  .alias("s")
  .description("problem to solve, giving JSON data as input. Provide path with 'sample' will use our sample data to demonstrate how this works")
  .action(async function (data) {
    try {
      let type = program.type;

      if (!type) {
        type = await prompt(questions);
      }

      switch (type.problem || type.toLowerCase()) {
        case 'shortest-route': {
          if (data.toLowerCase() === 'sample') {
            jsonFile.readFile("src/fakers/populations.json", function (error, json) {

              if (error) {
                console.info(chalk.yellow("Opps ! Something went wrong"));
                console.info(error);

                // mongoose.disconnect();
                process.exit(1);
              }

              if (json) {
                // const startTime = new Date ();
                const result = shortestRouteSolver(json.simpleIntData);
                // console.log(result);
                // console.info ( chalk.green ( ( new Date () ) - startTime ) );
              }

            });
          }

          break;
        }

      }

      console.log(chalk.magenta("Data: ") + chalk.green(data));
      console.log(chalk.magenta("Problem Type: " + chalk.green(type.problem || type)));
    } catch (error) {
      console.info(chalk.yellow("Opps ! Something went wrong"));
      console.info(error);

      // mongoose.disconnect();
      process.exit(1);
    }
    // mongoose.disconnect();
  });

program.parse(process.argv);

if (!program.args.length) {
  program.outputHelp();
  // mongoose.disconnect();
}